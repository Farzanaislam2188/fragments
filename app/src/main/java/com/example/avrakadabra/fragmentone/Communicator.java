package com.example.avrakadabra.fragmentone;

/**
 * Created by Java & Android on 1/19/2018.
 */

public interface Communicator {

    public void respond(String data);
}
