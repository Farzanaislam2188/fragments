package com.example.avrakadabra.fragmentone;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragmentOne extends Fragment {
 Button btnFragmentOne;
 EditText etFragmentOne;
  Communicator com;
    public BlankFragmentOne() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        com=(Communicator)context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_blank_fragment_one,container,false);
        etFragmentOne=rootView.findViewById(R.id.fragment_one_edt);
        btnFragmentOne=rootView.findViewById(R.id.fragment_one_button);

        btnFragmentOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data=etFragmentOne.getText().toString();
                com.respond(data);
            }
        });
        return rootView;
    }

}
