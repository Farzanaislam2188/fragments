package com.example.avrakadabra.fragmentone;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements Communicator {
       //  BlankFragmentTwo f2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BlankFragmentOne f1=new BlankFragmentOne();
        BlankFragmentTwo f2=new BlankFragmentTwo();

        android.app.FragmentManager fm=getFragmentManager();//calling fragment manager
        //fm.beginTransaction();//return fragment transaction
        FragmentTransaction ft=fm.beginTransaction();
        ft.add(R.id.container_fragment_one,f1,"first Fragment");
        ft.add(R.id.container_fragment_two,f2,"Second Fragment");
        ft.commit();








    }
    public void respond(String data)
    {
        // f2.changeData(data);
//        BlankFragmentTwo blankfragmenttwo=getFragmentManager().findFragmentByTag("Second Fragment");
//        bft.changeData(data);
//    }
       BlankFragmentTwo blankFragmentTwo= (BlankFragmentTwo) getFragmentManager().findFragmentByTag("Second Fragment");
       blankFragmentTwo.changeData(data);


    }
}
