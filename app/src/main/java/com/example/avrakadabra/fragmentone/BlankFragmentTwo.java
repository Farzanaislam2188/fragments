package com.example.avrakadabra.fragmentone;


import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragmentTwo extends Fragment {

   TextView textView2;
    public BlankFragmentTwo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootViewFragmentTwo=inflater.inflate(R.layout.fragment_blank_fragment_two,container,false);
       // View rootView=inflater.inflate(R.layout.fragment_blank_fragment_two,container,false);
        textView2=rootViewFragmentTwo.findViewById(R.id.textView2);

        // Inflate the layout for this fragment
       return rootViewFragmentTwo;
    }
    public void changeData(String data)
    {
        textView2.setText(data);
    }

}
